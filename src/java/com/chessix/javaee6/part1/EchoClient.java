package com.chessix.javaee6.part1;

import javax.ejb.EJB;

public class EchoClient {
 
    @EJB
    private EchoBean echoBean;
 
    public String reflect() {
        return echoBean.echo("What's your name?");
    }
}
