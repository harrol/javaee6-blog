package com.chessix.javaee6.part1;

import javax.ejb.Stateless;

@Stateless
public class EchoBean {
 
    public String echo(String input) {
        return input;
    }
 
}
